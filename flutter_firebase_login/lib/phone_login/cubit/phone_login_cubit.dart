import 'package:authentication_repository/authentication_repository.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:form_inputs/form_inputs.dart';
import 'package:formz/formz.dart';

part 'phone_login_state.dart';

class PhoneLoginCubit extends Cubit<PhoneLoginState> {
  PhoneLoginCubit(this._authenticationRepository)
      : super(const PhoneLoginState());

  final AuthenticationRepository _authenticationRepository;

  void phoneChanged(String value) {
    final phone = Phone.dirty(value);
    emit(state.copyWith(
      phone: phone,
      status: Formz.validate([
        phone,
      ]),
    ));
  }

  void smsCodeChanged(String value) {
    final smsCode = SmsCode.dirty(value);

    emit(state.copyWith(
      smsCode: smsCode,
      status: Formz.validate([
        smsCode,
      ]),
    ));
  }

  Future<void> verifyPhoneNumber() async {
    emit(state.copyWith(status: FormzStatus.submissionInProgress));
    try {
      await _authenticationRepository.verifyPhoneNumber(state.phone.value);
      emit(state.copyWith(status: FormzStatus.pure));
    } on Exception {
      emit(state.copyWith(status: FormzStatus.submissionFailure));
    } on NoSuchMethodError {
      emit(state.copyWith(status: FormzStatus.pure));
    }
  }

  Future<void> signInWithPhoneNumber() async {
    emit(state.copyWith(status: FormzStatus.submissionInProgress));
    try {
      await _authenticationRepository
          .signInWithPhoneNumber(state.smsCode.value);
      emit(state.copyWith(
          status: FormzStatus.submissionSuccess,));
    } on Exception {
      emit(state.copyWith(status: FormzStatus.submissionFailure));
    } on NoSuchMethodError {
      emit(state.copyWith(status: FormzStatus.pure));
    }
  }
}
