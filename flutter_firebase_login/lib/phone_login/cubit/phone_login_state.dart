part of 'phone_login_cubit.dart';

enum ConfirmPhoneValidationError { invalid }

class PhoneLoginState extends Equatable {
  const PhoneLoginState({
    this.phone = const Phone.pure(),
    this.smsCode =const SmsCode.pure(),
    this.status = FormzStatus.pure,
  });

  final Phone phone;
  final SmsCode smsCode;
  final FormzStatus status;
  
  @override
  List<Object> get props => [phone, smsCode, status];

  PhoneLoginState copyWith({
    Phone? phone,
    SmsCode? smsCode,
    FormzStatus? status,
  }) {
    return PhoneLoginState(
      phone: phone ?? this.phone,
      smsCode: smsCode ?? this.smsCode,
      status: status ?? this.status,
    );
  }
}
