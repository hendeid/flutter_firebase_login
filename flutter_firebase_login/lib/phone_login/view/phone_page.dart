import 'package:authentication_repository/authentication_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_firebase_login/phone_login/cubit/phone_login_cubit.dart';
import 'package:flutter_firebase_login/phone_login/view/phone_form.dart';

class PhoneLoginPage extends StatelessWidget {
  PhoneLoginPage({Key? key}) : super(key: key);

  static Route route() {
    return MaterialPageRoute<void>(builder: (_) => PhoneLoginPage());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Phone Login')),
      body: Padding(
        padding: const EdgeInsets.all(32.0),
        child: BlocProvider<PhoneLoginCubit>(
          create: (_) =>
              PhoneLoginCubit(context.read<AuthenticationRepository>()),
          child: PhoneLoginForm(),
        ),
      ),
    );
  }
}
