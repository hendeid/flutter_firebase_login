import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_firebase_login/login/login.dart';
import 'package:flutter_firebase_login/phone_login/cubit/phone_login_cubit.dart';
import 'package:flutter_firebase_login/phone_login/view/phone_page.dart';
import 'package:flutter_firebase_login/sign_up/sign_up.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:formz/formz.dart';
import 'package:sms_autofill/sms_autofill.dart';

class PhoneLoginForm extends StatelessWidget {
  PhoneLoginForm({Key? key}) : super(key: key);

  final SmsAutoFill _autoFill = SmsAutoFill();

  @override
  Widget build(BuildContext context) {
    return BlocListener<PhoneLoginCubit, PhoneLoginState>(
      listener: (context, state) {
        if (state.status.isSubmissionSuccess) {
          Navigator.of(context).pop();
        } else if (state.status.isSubmissionFailure) {
          ScaffoldMessenger.of(context)
            ..hideCurrentSnackBar()
            ..showSnackBar(
              const SnackBar(content: Text('Authentication Failure')),
            );
        }
      },
      child: SingleChildScrollView(
        child: Column(
          children: [
            const SizedBox(
              height: 32,
            ),
            _PhoneInput(),
            const SizedBox(
              height: 32,
            ),
            _VerifyPhoneButton(),
            const SizedBox(
              height: 32,
            ),
            _SmsCodeInput(),
            const SizedBox(
              height: 32,
            ),
            _SigninPhoneButton(),
          ],
        ),
      ),
    );
  }
}

class _PhoneInput extends StatelessWidget {
  final TextEditingController _phoneNumberController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PhoneLoginCubit, PhoneLoginState>(
      buildWhen: (previous, current) => previous.phone != current.phone,
      builder: (context, state) {
        return TextField(
            controller: _phoneNumberController,
            keyboardType: TextInputType.phone,
            style: TextStyle(fontSize: 14),
            onChanged: (phone) =>
                context.read<PhoneLoginCubit>().phoneChanged(phone),
            decoration: InputDecoration(
              contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 8),
              prefixText: '+49',
              labelText: "Phone number",
              errorText: state.phone.invalid ? 'invalid phone' : null,
              fillColor: Colors.white,
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(15.0),
                borderSide: BorderSide(
                  color: Color(0xFF00BCD4),
                ),
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(15.0),
                borderSide: BorderSide(
                  color: Color(0xFF00BCD4).withOpacity(0.5),
                  width: 2.0,
                ),
              ),
            ));
      },
    );
  }
}

class _SmsCodeInput extends StatelessWidget {
  final TextEditingController _smsController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PhoneLoginCubit, PhoneLoginState>(
      buildWhen: (previous, current) => previous.smsCode != current.smsCode,
      builder: (context, state) {
        return TextField(
            controller: _smsController,
            style: const TextStyle(fontSize: 14),
            keyboardType: TextInputType.number,
            onChanged: (smsCode) =>
                context.read<PhoneLoginCubit>().smsCodeChanged(smsCode),
            decoration: InputDecoration(
              contentPadding:
                  const EdgeInsets.symmetric(vertical: 0, horizontal: 8),
              labelText: 'Verification code',
              errorText: state.smsCode.invalid ? 'invalid smsCode' : null,
              fillColor: Colors.white,
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(25.0),
                borderSide: const BorderSide(
                  color: Color(0xFF00BCD4),
                ),
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(15.0),
                borderSide: BorderSide(
                  color: Color(0xFF00BCD4).withOpacity(0.5),
                  width: 2.0,
                ),
              ),
            ));
      },
    );
  }
}

class _VerifyPhoneButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PhoneLoginCubit, PhoneLoginState>(
      buildWhen: (previous, current) => previous.status != current.status,
      builder: (context, state) {
        return ElevatedButton(
            key: const Key('verify_PHONE_raisedButton'),
            style: ElevatedButton.styleFrom(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0),
              ),
              primary: const Color(0xFFFFD600),
            ),
            onPressed: state.status.isValidated
                ? () => context.read<PhoneLoginCubit>().verifyPhoneNumber()
                : null,
            child: const Text(
              'Verify Number',
              style: TextStyle(color: Colors.white),
            ));
      },
    );
  }
}

class _SigninPhoneButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PhoneLoginCubit, PhoneLoginState>(
      buildWhen: (previous, current) => previous.status != current.status,
      builder: (context, state) {
        return state.status.isSubmissionInProgress
            ? const CircularProgressIndicator()
            : ElevatedButton(
                style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  primary: const Color(0xFF00BCD4),
                ),
                onPressed: state.status.isValidated
                    ? () {
                        if (state.phone.value.isNotEmpty) {
                          context
                              .read<PhoneLoginCubit>()
                              .signInWithPhoneNumber();
                        } else {
                          ScaffoldMessenger.of(context)
                            ..hideCurrentSnackBar()
                            ..showSnackBar(
                              const SnackBar(content: Text('ADD YOUR PHONE ')),
                            );
                        }
                      }
                    : null,
                child: const Text(
                  'Sign in',
                  style: TextStyle(color: Colors.white),
                ));
      },
    );
  }
}
