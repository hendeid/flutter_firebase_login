import 'package:formz/formz.dart';

enum SMSCodeValidationError { invalid }

class SmsCode extends FormzInput<String, SMSCodeValidationError> {
  const SmsCode.pure() : super.pure('');
  const SmsCode.dirty([String value = '']) : super.dirty(value);

  static final RegExp _smsRegExp = RegExp(
    "^[0-9]*",
  );

  @override
  SMSCodeValidationError? validator(String? value) {
    return _smsRegExp.hasMatch(value ?? '')
        ? null
        : SMSCodeValidationError.invalid;
  }
}
